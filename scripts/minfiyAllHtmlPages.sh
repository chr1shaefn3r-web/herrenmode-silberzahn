#!/usr/bin/env bash

for f in dist/*.html; do
	echo "> Minify $f:";
	html-minifier -c configs/html-minifier.conf -o $f $f
done

