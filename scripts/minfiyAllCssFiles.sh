#!/usr/bin/env bash

for f in dist/css/*.css; do
	echo "> Minify $f";
	cssnano $f $f
done

