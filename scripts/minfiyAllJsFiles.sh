#!/usr/bin/env bash

for f in dist/js/*.js; do
	echo "> Minify $f";
	uglifyjs -c -o $f $f
done

