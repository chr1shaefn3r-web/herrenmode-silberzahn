module.exports = function(grunt) {
	require('time-grunt')(grunt);
	grunt.initConfig({
		responsive_images: {
			options: {
				engine: "im",
				quality: 85
			},
			background: {
				options: {
					sizes: [{
						name: 'large',
						rename: false,
						width: 2560
					},{
						name: 'medium',
						width: 992
					},{
						name: 'small',
						width: 768
					}]
				},
				files: [{
					expand: true,
					cwd: './',
					src: ['images/background/*'],
					dest: 'dist/'
				}]
			}
		},
		autoshot: {
			options: {
				path: "screenshots",
			},
			homepage: {
				options: {
					remote: {
						files: []
					},
					local: {
						path: "dist/",
						port: 9000,
						files: [
							{src: "index.html", dest: "home.jpg"},
							{src: "service.html", dest: "service.jpg"},
							{src: "anfahrt.html", dest: "anfahrt.jpg"},
							{src: "impressum.html", dest: "impressum.jpg"}
						]
					},
					viewport: ['2880x1800'/*15"Retina*/, '2560x1600'/*WQXGA*/, '1920x1080'/*FullHD*/, '800x1290'/*7"Tablet*/, '480x800'/*HTC Desire*/]
				}
			}
		}
	});

	require('load-grunt-tasks')(grunt);
};

