// Navbar and dropdowns
var toggle = document.getElementsByClassName('navbar-toggle')[0],
    collapse = document.getElementsByClassName('navbar-collapse')[0];

// Toggle if navbar menu is open or closed
function toggleMenu() {
    collapse.classList.toggle('collapse');
    collapse.classList.toggle('in');
}

// Close dropdowns when screen becomes big enough to switch to open by hover
function closeMenusOnResize() {
    if (document.body.clientWidth >= 768) {
        collapse.classList.add('collapse');
        collapse.classList.remove('in');
    }
}

// Event listeners
window.addEventListener('resize', closeMenusOnResize, false);
toggle.addEventListener('click', toggleMenu, false);

// Image slider
if (document.getElementById('sImgNavHolderO')) {
	var total = 5,
		delay = 5000,
		current = 0,
		previous = total - 1,
		load = 1,
		loaded = [true],
		goToNext = false;
    for (i = 1; i < total; i++) {
		loaded[i] = false;
	}
    document.getElementById('sImgNavHolderO').style.display = 'block';
    window.addEventListener('load', imgLoad, false);
}

function imgLoad() {
    if(1 == load) {
		setTimeout(nextImg, delay);
	}
    var a = document.getElementById('sImg' + load);
    a.onload = function() {
        loaded[load] = true;
        if(goToNext) {
			(goToNext = false, nextImg());
		}
        load++;
        if(load < total) {
			imgLoad();
		}
    };
    a.src = a.getAttribute('data-src');
}

function nextImg() {
    var a = (current + 1) % total;
    loaded[a] ? (document.getElementById('sImgNav' + current).style.opacity = 0.5, document.getElementById('sImgNav' + a).style.opacity = 1, 0 != a ? document.getElementById('sImg' + a).style.opacity = 1 : document.getElementById('sImg' + current).style.opacity = 0, a == total - 1 && (document.getElementById('sImg0').style.opacity = 1), previous = current, current = a, setTimeout(nextImg, delay), setTimeout(hidePrevImg, delay / 2)) : goToNext = !0
}

function hidePrevImg() {
    document.getElementById('sImg' + previous).style.opacity = 0;
}

